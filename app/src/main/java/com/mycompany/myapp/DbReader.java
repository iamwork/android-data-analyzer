package com.mycompany.myapp2;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.util.Log;
import java.util.ArrayList;


public class DbReader
{
	String TAG = "DataBase";
	
	
	private SQLiteDatabase db;
    private Context cont;
	
    public DbReader(Context context) {
        
        db = new DbHelper(context).getWritableDatabase();
        cont=context;
    }
	
	
    public void insertData()
    {
	
	Log.d(TAG, "--- Insert in sensors: ---");
	// подготовим данные для вставки в виде пар: наименование столбца - значение
	// создаем объект для данных
	ContentValues cv = new ContentValues();
	
	//cv.put("id", "*");
	for(int i = 2; i< 16; i++){
	
	    cv.put("place", "A" + String.valueOf(i));
		cv.put("name", "*");
		cv.put("parameters", "*");
		cv.put("other", "*");
		
		long rowID = db.insert("sensors", null, cv);
		Log.d(TAG, "row inserted, ID = " + rowID);
		
	}

		for(int i = 0; i< 16; i++){

			cv.put("place", "B" + String.valueOf(i));
			cv.put("name", "*");
			cv.put("parameters", "*");
			cv.put("other", "*");

			long rowID = db.insert("sensors", null, cv);
			Log.d(TAG, "row inserted, ID = " + rowID);

		}
	
		for(int i = 0; i< 16; i++){

			cv.put("place", "C" + String.valueOf(i));
			cv.put("name", "*");
			cv.put("parameters", "*");
			cv.put("other", "*");

			long rowID = db.insert("sensors", null, cv);
			Log.d(TAG, "row inserted, ID = " + rowID);

		}
	
	
		for(int i = 0; i< 16; i++){

			cv.put("place", "D" + String.valueOf(i));
			cv.put("name", "*");
			cv.put("parameters", "*");
			cv.put("other", "*");

			long rowID = db.insert("sensors", null, cv);
			Log.d(TAG, "row inserted, ID = " + rowID);

		}

	
		for(int i = 0; i< 16; i++){

			cv.put("place", "E" + String.valueOf(i));
			cv.put("name", "*");
			cv.put("parameters", "*");
			cv.put("other", "*");

			long rowID = db.insert("sensors", null, cv);
			Log.d(TAG, "row inserted, ID = " + rowID);

		}
	
		
		for(int i = 0; i< 16; i++){

			cv.put("place", "F" + String.valueOf(i));
			cv.put("name", "*");
			cv.put("parameters", "*");
			cv.put("other", "*");

			long rowID = db.insert("sensors", null, cv);
			Log.d(TAG, "row inserted, ID = " + rowID);

		}
		

		for(int i = 0; i< 16; i++){
			
			cv.put("place", "G" + String.valueOf(i));
			cv.put("name", "*");
			cv.put("parameters", "*");
			cv.put("other", "*");

			long rowID = db.insert("sensors", null, cv);
			Log.d(TAG, "row inserted, ID = " + rowID);

		}	
		
	// вставляем запись и получаем ее ID
	long rowID = db.insert("sensors", null, cv);
	Log.d(TAG, "row inserted, ID = " + rowID);
	
	}
	
	
    public ArrayList<String> getData(int position)
    {
		ArrayList<String> list = new ArrayList<String>();
		Cursor c = db.rawQuery("SELECT * FROM sensors WHERE id = " + String.valueOf(position), null);
		
		if (c.moveToFirst()) {

			do {
				Log.d(TAG,
					  "id = " + c.getInt(0) + " / " +
					  ", place = " + c.getString(1) + " / " +
					  ", name = " + c.getString(2) + " / " +
					  ", parameters = " + c.getString(3) + " / " +
					  ", other = " + c.getString(4) 

					  );
					  
				list.add(c.getString(0));		
				list.add(c.getString(1)); 	  
				list.add(c.getString(2));
				list.add(c.getString(3)); 	
				list.add(c.getString(4)); 	

			} while (c.moveToNext());
	
		}
			
		c.close();
		db.close();
		
		return list;
	}
 	
	
}

