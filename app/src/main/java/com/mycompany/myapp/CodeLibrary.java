package com.mycompany.myapp2;

import android.app.*;
import android.bluetooth.*;
import android.content.*;
import android.os.*;
import android.util.*;
import android.view.*;
import android.widget.*;
import java.io.*;
import java.util.*;
import java.util.Locale;
import java.nio.charset.*;
import java.sql.*;


public class CodeLibrary
{
	private static final String TAG = "Bl2";
	
	
	public boolean check_Summa (ArrayList<String> read_data){
		
		boolean rezult = false;
		int summa = 0;
	
		String crc = read_data.get(4) + read_data.get(5);
		
		for (int i = 0; i< 4; i++){

		  summa += Integer.decode("0x" +read_data.get(i));	
		}	
	
		if(summa == Integer.decode("0x" +crc)){rezult = true;} 
		
		Log.d(TAG, "code lib 2:"  + summa +  " - "  + Integer.decode("0x" +crc));	 
		
		return rezult;
	}
	
	
	public String port_name(ArrayList<String> read_data){
		
		String rezult = "*";
		
		Log.d(TAG, "code lib:"+  read_data.get(3));
		
		if(read_data.get(3).equals("0a")){rezult = "A";}
		if(read_data.get(3).equals("0b")){rezult = "B";}
		if(read_data.get(3).equals("0c")){rezult = "C";}
		if(read_data.get(3).equals("0d")){rezult = "D";}
		if(read_data.get(3).equals("0e")){rezult = "E";}
		if(read_data.get(3).equals("0f")){rezult = "F";}
		if(read_data.get(3).equals("f2")){rezult = "G";}
		
		return rezult;
		
	} 
	
	public ArrayList<String> port_data(ArrayList<String> read_data){
	
		ArrayList<String> rezult = new ArrayList<String>();
		int reg = (Integer.decode( "0x" + read_data.get(2) +read_data.get(1) )  );
		int register[] = {0x0001,0x0002,0x0004,0x0008,0x0010,0x0020,0x0040,0x0080,0x0100,0x0200, 
			0x0400,0x0800,0x1000,0x2000,0x4000, 0x8000};
		
		for(int i = 0; i < 16; i++ ){
			
			if ((reg & register[i]) == 0){
		
				rezult.add("0");
	
			}else rezult.add("1");
			   
			   
		}
		
		
		Log.d(TAG, "reg:" + reg + " / " + register[4] + " / " +(reg & register[4]) );	
		
		
	    return rezult;
	}
	
}
