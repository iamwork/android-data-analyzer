package com.mycompany.myapp2;

import android.app.*;
import android.bluetooth.*;
import android.content.*;
import android.os.*;
import android.util.*;
import android.view.*;
import android.widget.*;
import java.io.*;
import java.util.*;
import java.util.Locale;
import java.nio.charset.*;
import java.lang.StringBuilder.*;



public class MainActivity extends Activity 
{
	private static final String TAG = "Bl";
	boolean start_bl = false;
	final int RECIEVE_MESSAGE = 1;
	final int ALARM_MESSAGE = 2;
	int port = 0;
	Handler h;
	//ArrayList<String> buf_read = new ArrayList<String>();
	ArrayList<String> indictors =  new ArrayList<String>();
	byte buf_write[] = new byte [6];
	byte buf_A[] = {(byte)0xF0,(byte)0xD2,(byte)0x16,(byte)0x0A,(byte)0x01,(byte)0xE2};
	byte buf_B[] = {(byte)0xF0,(byte)0xD2,(byte)0x16,(byte)0x0B,(byte)0x01,(byte)0xE3};
	byte buf_C[] = {(byte)0xF0,(byte)0xD2,(byte)0x16,(byte)0x0C,(byte)0x01,(byte)0xE4};
	byte buf_D[] = {(byte)0xF0,(byte)0xD2,(byte)0x16,(byte)0x0D,(byte)0x01,(byte)0xE5};
	byte buf_E[] = {(byte)0xF0,(byte)0xD2,(byte)0x16,(byte)0x0E,(byte)0x01,(byte)0xE6};
	byte buf_F[] = {(byte)0xF0,(byte)0xD2,(byte)0x16,(byte)0x0F,(byte)0x01,(byte)0xE7};
	byte buf_G[] = {(byte)0xF0,(byte)0xD2,(byte)0x16,(byte)0xF2,(byte)0x02,(byte)0xCA};
	
	
	BluetouthLibrary bl = new BluetouthLibrary();
    Timer timer = new Timer();
	
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

		Toast.makeText(getBaseContext(), "Start...", Toast.LENGTH_LONG).show();	
		
	
		timer.schedule(new UpdateTimeTask(), 0, 1000);
		
		for(int i = 0; i < 16; i++ ){

			indictors.add("0");

		}
		

		DbHelper helper = new DbHelper(this);
		helper.changeDataBase();
		
	
		TextView t_add = findViewById(R.id.add);

		t_add.setText(

		 "Select port and pin of the signal... " 

		);


		h = new Handler() {
			
			TextView Msg = (TextView) findViewById(R.id.t_mes);
			//TextView Msg_main = (TextView) findViewById(R.id.t_main);
			Button Led_1 = (Button)findViewById(R.id.Led_1);
			Button Led_2 = (Button)findViewById(R.id.Led_2);
			Button Led_3 = (Button)findViewById(R.id.Led_3);
			Button Led_4 = (Button)findViewById(R.id.Led_4);
			Button Led_5 = (Button)findViewById(R.id.Led_5);
			Button Led_6 = (Button)findViewById(R.id.Led_6);
			Button Led_7 = (Button)findViewById(R.id.Led_7);
			Button Led_8 = (Button)findViewById(R.id.Led_8);
			Button Led_9 = (Button)findViewById(R.id.Led_9);
			Button Led_10 = (Button)findViewById(R.id.Led_10);
			Button Led_11 = (Button)findViewById(R.id.Led_11);
			Button Led_12 = (Button)findViewById(R.id.Led_12);
			Button Led_13 = (Button)findViewById(R.id.Led_13);
			Button Led_14 = (Button)findViewById(R.id.Led_14);
			Button Led_15 = (Button)findViewById(R.id.Led_15);
			Button Led_16 = (Button)findViewById(R.id.Led_16);


			public void handleMessage(android.os.Message msg) {
				
			
				if (msg.what == ALARM_MESSAGE){			
					Msg.setText("Resive: " + msg.obj.toString()); 		
				}
				
							
				if (msg.what == RECIEVE_MESSAGE){
					
					String state = msg.obj.toString().toUpperCase();
				Msg.setText("Resive: " + state); 	

				        Led_1.setBackgroundColor(0XFFFF8CD6F9);
						Led_2.setBackgroundColor(0XFFFF8CD6F9);
						Led_3.setBackgroundColor(0XFFFF8CD6F9);
						Led_4.setBackgroundColor(0XFFFF8CD6F9);
						Led_5.setBackgroundColor(0XFFFF8CD6F9);
						Led_6.setBackgroundColor(0XFFFF8CD6F9);
						Led_7.setBackgroundColor(0XFFFF8CD6F9);
						Led_8.setBackgroundColor(0XFFFF8CD6F9);
						Led_9.setBackgroundColor(0XFFFF8CD6F9);
						Led_10.setBackgroundColor(0XFFFF8CD6F9);
						Led_11.setBackgroundColor(0XFFFF8CD6F9);
						Led_12.setBackgroundColor(0XFFFF8CD6F9);
						Led_13.setBackgroundColor(0XFFFF8CD6F9);
						Led_14.setBackgroundColor(0XFFFF8CD6F9);
						Led_15.setBackgroundColor(0XFFFF8CD6F9);
						Led_16.setBackgroundColor(0XFFFF8CD6F9);


						if(indictors.get(0).equals("1")){Led_1.setBackgroundColor(0XFFFFF78CE4);}
						if(indictors.get(1).equals("1")){Led_2.setBackgroundColor(0XFFFFF78CE4);}
						if(indictors.get(2).equals("1")){Led_3.setBackgroundColor(0XFFFFF78CE4);}
						if(indictors.get(3).equals("1")){Led_4.setBackgroundColor(0XFFFFF78CE4);}
						if(indictors.get(4).equals("1")){Led_5.setBackgroundColor(0XFFFFF78CE4);}
						if(indictors.get(5).equals("1")){Led_6.setBackgroundColor(0XFFFFF78CE4);}
						if(indictors.get(6).equals("1")){Led_7.setBackgroundColor(0XFFFFF78CE4);}
						if(indictors.get(7).equals("1")){Led_8.setBackgroundColor(0XFFFFF78CE4);}
						if(indictors.get(8).equals("1")){Led_9.setBackgroundColor(0XFFFFF78CE4);}
						if(indictors.get(9).equals("1")){Led_10.setBackgroundColor(0XFFFFF78CE4);}
						if(indictors.get(10).equals("1")){Led_11.setBackgroundColor(0XFFFFF78CE4);}
						if(indictors.get(11).equals("1")){Led_12.setBackgroundColor(0XFFFFF78CE4);}
						if(indictors.get(12).equals("1")){Led_13.setBackgroundColor(0XFFFFF78CE4);}
						if(indictors.get(13).equals("1")){Led_14.setBackgroundColor(0XFFFFF78CE4);}
						if(indictors.get(14).equals("1")){Led_15.setBackgroundColor(0XFFFFF78CE4);}
						if(indictors.get(15).equals("1")){Led_16.setBackgroundColor(0XFFFFF78CE4);}
					}
				
					
					};			
					
		};
		

	}

	@Override
	protected void onDestroy()
	{
		// TODO: Implement this method
		super.onDestroy();
		
		bl.Close();
	};
	
	
	class UpdateTimeTask extends TimerTask{
		
		CodeLibrary lib = new CodeLibrary();

	
		public void run(){
	
			
			if(!start_bl){start_bl = true; bl.chBTState(); if(bl.ch_connect()){
				
			bl.Start_Bl();
			h.obtainMessage(ALARM_MESSAGE, 1, -1, "Connect exist... " ).sendToTarget();} 
			
			}	
			
			
			if(!bl.ch_connect()){h.obtainMessage(ALARM_MESSAGE, 1, -1, "Connect is not exist... " ).sendToTarget();}
			
				
			if(bl.Resive().size() == 6){

				if(lib.check_Summa(bl.Resive())){
					
					Log.d(TAG,"check_Summa = true" );
					
					ArrayList<String> buf_read = new ArrayList<String>(bl.Resive());
	
					indictors = (lib.port_data(buf_read));			
									
					h.obtainMessage(RECIEVE_MESSAGE, 1, -1, buf_read ).sendToTarget();

					bl.Clear();
				}	
			
			}				
			
			Log.d(TAG,"Timer!" + bl.Resive());
			
		}
		
	}
	
	
	
	
	public void OnClick(View view) {

		TextView textView = findViewById(R.id.t_mes);
		TextView t_add = findViewById(R.id.add);
		t_add.setText("");
		
		Button btn_A = (Button) findViewById(R.id.btn_A);
		btn_A.setBackgroundColor(0XFFFF9EA6ED);

		Button btn_B = (Button) findViewById(R.id.btn_B);
		btn_B.setBackgroundColor(0XFFFF9EA6ED);		

		Button btn_C = (Button) findViewById(R.id.btn_C);
		btn_C.setBackgroundColor(0XFFFF9EA6ED);

		Button btn_D = (Button) findViewById(R.id.btn_D);
		btn_D.setBackgroundColor(0XFFFF9EA6ED);			

		Button btn_E = (Button) findViewById(R.id.btn_E);
		btn_E.setBackgroundColor(0XFFFF9EA6ED);

		Button btn_F = (Button) findViewById(R.id.btn_F);
		btn_F.setBackgroundColor(0XFFFF9EA6ED);

		Button btn_G = (Button) findViewById(R.id.btn_G);
		btn_G.setBackgroundColor(0XFFFF9EA6ED);

		Button btn_Next = (Button) findViewById(R.id.btn_Next);
		btn_Next.setBackgroundColor(0XFFFF9EA6ED);	

		Button btn_Send = (Button) findViewById(R.id.btn_Send);
		btn_Send.setBackgroundColor(0XFFFF9EA6ED);



		switch(view.getId()){

			case R.id.btn_A:

				textView.setText("PORT A: Read discrets....");
				view.setBackgroundColor(0XFFFFCDD1F9);
				buf_write = buf_A;
				port  = 1;

				break;

			case R.id.btn_B:

				textView.setText("PORT B: Read discrets....");
				view.setBackgroundColor(0XFFFFCDD1F9);
				buf_write = buf_B;
                port  = 2;
				
				break;

			case R.id.btn_C:

				textView.setText("PORT C: Read discrets....");
				view.setBackgroundColor(0XFFFFCDD1F9);
				buf_write = buf_C;
				port  = 3;

				break;  

			case R.id.btn_D:

				textView.setText("PORT D: Read discrets....");
				view.setBackgroundColor(0XFFFFCDD1F9);
				buf_write = buf_D;
				port  = 4;

				break;

			case R.id.btn_E:

				textView.setText("PORT E: Read discrets....");
				view.setBackgroundColor(0XFFFFCDD1F9);
				buf_write = buf_E;
				port  = 5;
				
				break;  

			case R.id.btn_F:

				textView.setText("PORT F: Read discrets....");
				view.setBackgroundColor(0XFFFFCDD1F9);
				buf_write = buf_F;
                port  = 6;
				
				break;

			case R.id.btn_G:

				textView.setText("PORT G: Read discrets....");
				view.setBackgroundColor(0XFFFFCDD1F9);
				buf_write = buf_G;
				port  = 7;

				break;  

			case R.id.btn_Next:

				textView.setText("Next: Read analogs....");
				view.setBackgroundColor(0XFFFFCDD1F9);

				Intent intent = new Intent(this, SubActivity.class);
				startActivity(intent);

				break;  

		}
	}


	public void OnSendClick(View view) {

	  if(bl.ch_connect()){bl.Send(buf_write);}

	}



	public void OnPinsClick(View view) {
		
		
		Button Led_1 = (Button)findViewById(R.id.Led_1);
		Button Led_2 = (Button)findViewById(R.id.Led_2);
		Button Led_3 = (Button)findViewById(R.id.Led_3);
		Button Led_4 = (Button)findViewById(R.id.Led_4);
		Button Led_5 = (Button)findViewById(R.id.Led_5);
		Button Led_6 = (Button)findViewById(R.id.Led_6);
		Button Led_7 = (Button)findViewById(R.id.Led_7);
		Button Led_8 = (Button)findViewById(R.id.Led_8);
		Button Led_9 = (Button)findViewById(R.id.Led_9);
		Button Led_10 = (Button)findViewById(R.id.Led_10);
		Button Led_11 = (Button)findViewById(R.id.Led_11);
		Button Led_12 = (Button)findViewById(R.id.Led_12);
		Button Led_13 = (Button)findViewById(R.id.Led_13);
		Button Led_14 = (Button)findViewById(R.id.Led_14);
		Button Led_15 = (Button)findViewById(R.id.Led_15);
		Button Led_16 = (Button)findViewById(R.id.Led_16);
		
		
	DbReader reader  = new DbReader(this);
	
	int position = 0;
	int move = (port-1) *16;
	
	
	if(port  > 0){
			
		switch(view.getId()){
			

			case R.id.Led_1:
				
				position = 1 + move;
	
				break;  
				
			case R.id.Led_2:

				position = 2 + move;

				break;  
				
			case R.id.Led_3:

				position = 3 + move;

				break;  
				
			case R.id.Led_4:

				position = 4 + move;

				break;  
				
			case R.id.Led_5:

				position = 5 + move;

				break;  
				
			case R.id.Led_6:

				position = 6 + move;

				break;  
				
			case R.id.Led_7:

				position = 7 + move;

				break;  
				
			case R.id.Led_8:

				position = 8 + move;

				break;  
				
			case R.id.Led_9:

				position = 9 + move;

				break;  
				
			case R.id.Led_10:

				position = 10 + move;

				break;  
				
			case R.id.Led_11:

				position = 11 + move;

				break;  
				
			case R.id.Led_12:

				position = 12 + move;

				break;  
				
			case R.id.Led_13:

				position = 13 + move;

				break;  
				
			case R.id.Led_14:

				position = 14 + move;

				break;  
				
			case R.id.Led_15:

				position = 15 + move;

				break;  
				
				
			case R.id.Led_16:

				position = 16 + move;

				break;  
				
	   
	}
	
		
	
	ArrayList<String> data =  reader.getData(position);

	Log.d(TAG, "Recive: " + data.toString());

	TextView t_add = findViewById(R.id.add);
	
	t_add.setText(
			
			"place: " +data.get(1).toString() + "\n" + 
			"name: " + data.get(2).toString() + "\n" +
			"parameters: " + data.get(3).toString() + "\n" +
			"connector: " + data.get(4).toString() + "\n"
	);
	
	
	}
		
	
	}

}


