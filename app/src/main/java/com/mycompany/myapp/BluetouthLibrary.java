package com.mycompany.myapp2;
import android.app.*;
import android.bluetooth.*;
import android.content.*;
import android.os.*;
import android.util.*;
import android.view.*;
import android.widget.*;
import java.io.*;
import java.util.*;
import java.util.Locale;
import java.nio.charset.*;
import java.sql.*;
import java.lang.StringBuilder.*;
import android.companion.*;



public class BluetouthLibrary
{
	Handler hahd;
	private BluetoothAdapter btAdapter = null;
	private BluetoothSocket btSocket = null;
	boolean connect_exist = false;
	boolean close_socket = false;
	private static final String TAG = "Bl";
	private ConnectedThread mConnectedThread;
	
	
	private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
	private static String address = "98:D3:31:F4:1D:5B";
	ArrayList<String> read_data = new ArrayList<String>();
	ArrayList<String> data_ch6 = new ArrayList<String>();
	byte buf_write_data[] =  {(byte)0xF0,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x00};;
	
	public void chBTState() {	

		btAdapter = BluetoothAdapter.getDefaultAdapter();

		Log.d(TAG, "getDefaultAdapter: " + btAdapter);
	
		   if (btAdapter.isEnabled()) {
			   
			   Log.d(TAG, "Bluetooth IS ENABLE   " + btAdapter.getName() + "   " +btAdapter.getState() );	   
			   
			   Set<BluetoothDevice> pairDevices = btAdapter.getBondedDevices();
	   
			   
			   if(pairDevices.size() > 0){		   
				   
				   for(BluetoothDevice device:pairDevices){
					   
					   Log.d(TAG, "pairDevices:  " + device.getName() + device.getAddress());
					  
					   if(device.getName().equals("<TECHNO")){
						   
						   btAdapter.cancelDiscovery();
						   
						   Log.d(TAG, "cancelDiscovery()");

						   
						   
						    device = btAdapter.getRemoteDevice(address);
						  
						   try {
							     btSocket = device.createRfcommSocketToServiceRecord(MY_UUID);
							     Log.d(TAG,"Create Socket");   
			

							   Log.d(TAG, "CONECTION...");
							
								   btSocket.connect();					  
								   connect_exist = true;
	 
							   
						   } catch (IOException e) {
							     Log.d(TAG,"Error create Socket!");
							     connect_exist = false;
								 
							   try {
								   btSocket.close();
								   Log.d(TAG, "Socket close!");
							   } catch (IOException e1) {
								   Log.d(TAG, "Socket close error!!!");
							   }
								 
						   }

						     
						   }
				   } 
				   
				   
			   }
			   
			      
				
		   } else{Log.d(TAG, "Bluetooth IS DISABLE!!!  " + btAdapter.getName() + "   " +btAdapter.getState() );
			
		      }
		
	     }



	private class ConnectedThread extends Thread {

	    private BluetoothSocket mmSocket;
	    private InputStream mmInStream;
	    private OutputStream mmOutStream;


	    public ConnectedThread(BluetoothSocket socket) {
	        
			
			mmSocket = socket;
	        InputStream tmpIn = null;
	        OutputStream tmpOut = null;

	        try {
	            tmpIn = socket.getInputStream();
	            tmpOut = socket.getOutputStream();
	        } catch (IOException e) { }

	        mmInStream = tmpIn;
	        mmOutStream = tmpOut;
			


	    }

	
	    public void run() {


	        while (true) {
				
	        	try {

					if(read_data.size() > 6){read_data.clear();};	

					read_data.add(String.format("%02x",mmInStream.read()));	

					Log.d(TAG, "+resive:"+ read_data);

					if(read_data.size() == 6){

						data_ch6 = read_data; 
					}


				} catch (IOException e) {
					break;
				}


	        }
			
			
	    }


		public void write(byte buf_write[]) {

	    	Log.d(TAG, "Write " + buf_write);

	    	try {
				mmOutStream.write(buf_write);
	        } catch (IOException e) {
	            Log.d(TAG, "WRITE ERR: " + e.getMessage()); 
			}
			
	    }

	}
	

	public void Send(byte buf_write[]) {
		
		if(buf_write.length == 6){
		
		buf_write_data = buf_write;
		}
		
		mConnectedThread.write(buf_write_data);

	}
	
	
	ArrayList<String>  Resive() {

		return data_ch6;
	}
	
	
	public void Clear(){
		
		data_ch6.clear();
		read_data.clear();
	}
	
	
	boolean ch_connect(){
	
	 return connect_exist;
	
	}
	
	
	public void Start_Bl(){
	
	 mConnectedThread = new ConnectedThread(btSocket);
	 mConnectedThread.start();
	 
	 Log.d(TAG, "Start_Bl");
	 
	}
	
	
	public void Close(){
		
	close_socket = true;
	
	if(btSocket != null){

		try {
			btSocket.close();
			Log.d(TAG, "Close Socket!");
		} catch (IOException e) {
			Log.d(TAG, "Close Socket error!!!");
		}
		
	}
	
	
	Log.d(TAG, "App close!");
	
	}


}
